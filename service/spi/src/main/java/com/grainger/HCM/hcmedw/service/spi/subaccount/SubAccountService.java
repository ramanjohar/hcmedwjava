// package com.grainger.HCM.hcmedw.service.spi.subaccount;

// import com.grainger.starter.boot.exception.RequestValidationException;
// import com.grainger.HCM.hcmedw.service.spi.subaccount.model.SubAccount;
// import org.springframework.data.domain.Page;
// import org.springframework.data.domain.Pageable;

// import java.util.Optional;

// public interface SubAccountService {

//   SubAccount add(SubAccount resource) throws RequestValidationException;

//   Page<SubAccount> findByLastName(String lastName, Pageable pageable);

//   Optional<SubAccount> findByUserName(String userName);

//   Optional<SubAccount> findById(String id);

//   Page<SubAccount> findAll(Pageable pageable);

//   Optional<SubAccount> updateById(String id, SubAccount record) throws RequestValidationException;

//   Optional<SubAccount> deleteById(String id);
// }
