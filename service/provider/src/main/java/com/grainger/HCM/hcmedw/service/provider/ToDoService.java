package com.grainger.HCM.hcmedw.service.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grainger.HCM.hcmedw.persistence.model.ToDoItem;
import com.grainger.HCM.hcmedw.persistence.model.ToDoItemRepository;

@Service
public class ToDoService {
    
    @Autowired
    private ToDoItemRepository toDoItemRepository;


    public Iterable<ToDoItem> findAll() {
        
        return toDoItemRepository.findAll();
    }


    public ToDoItem save(ToDoItem toDoItem) {

        return toDoItemRepository.save(toDoItem);
    }

}
