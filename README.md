# hcmedw

- [hcmedw](#hcmedw)
  - [About](#about)
  - [Getting Started](#getting-started)
    - [Required Dependencies](#required-dependencies)
    - [Verifying the Application is Running](#verifying-the-application-is-running)
  - [What's Next](#whats-next)

---

## About

This is a Java Spring Boot application that has been created to guide teams on how to build scalable java applications at Grainger.
It is intended to be used as a starting point for building Java APIs and should be customized to deliver whatever functionality is required.

## Getting Started
### Required Dependencies

Before you run this application locally, you will need to make sure you have all the following required dependencies available in your local environment:

- java
- docker
- haodolint
- shellcheck

>This application currently supports Mac OS for local development environments.
> Use the Mac OS Guide to make sure you have all the above dependencies available in your local environment.
> Otherwise, refer to the Other Operating Systems Guide.

```

### Running the Application

Once you have all the required dependencies, you can start the application in your local environment by navigating to the root of your application directory and running the following command:

```bash
./gradlew bootJar
```

If you face any error like cookiecutter plugin missing, run these commands

```

export AWS_DEFAULT_PROFILE=productcore-prod
export CODEARTIFACT_AUTH_TOKEN=`aws-vault exec ${AWS_DEFAULT_PROFILE} -- aws codeartifact get-authorization-token --region us-east-2 --domain grainger --query authorizationToken --output text`

```

This will build all the application artifacts and docker images

You can then start the application by running:

```bash

docker build -f Dockerfile -t {image tag} .

helm upgrade --install hcmedw ./deployment/helm -f ./deployment/helm/values-local.yaml 

```

> Note that at this time, `./gradlew run` and `./gradlew bootRun` require additional setup with database dependencies prior to use with a local development environment.

### Verifying the Application is Running

You can verify that the application is up and running by issuing the following commands in your terminal:

```bash
curl localhost:8081/actuator/health
curl localhost:8081/actuator/info
```

You should get back responses similar to the following:

```bash
curl localhost:8081/actuator/health

{
    "components": {
        "db": {
            "details": {
                "database": "PostgreSQL",
                "validationQuery": "isValid()"
            },
            "status": "UP"
        },
        "diskSpace": {
            "details": {
                "exists": true,
                "free": 48291934208,
                "threshold": 10485760,
                "total": 62725623808
            },
            "status": "UP"
        },
        "livenessState": {
            "status": "UP"
        },
        "ping": {
            "status": "UP"
        },
        "readinessState": {
            "status": "UP"
        }
    },
    "groups": [
        "liveness",
        "readiness"
    ],
    "status": "UP"
}
```

```bash
curl localhost:8081/actuator/info

{
    "app": {
        "description": "Java API Starter from Template",
        "name": "hcmedw"
    }
}
```

## What's Next

Once you have verified that you are able to run the application successfully, you can now start customizing the application to deliver the functionality you would like.

By default, this application assumes the use of a build, test, release cycle as defined in this development guide.
Take a look at that guide to see how you can make changes, test them and get them deployed to a target environment.

The application itself is organized into the following three tiers of functionality:

- API / Controller
- Service (business logic)
- Persistence