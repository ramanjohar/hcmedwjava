# Swagger Quickstart Guide

Guide for API testing and creating documentation via Swagger

## What is Swagger?

Swagger is an open-source framework for designing and describing APIs.

Swagger provides a set of tools that help programmers generate client or server code and install self-generated documentation for web services.

### Use Cases

Swagger can be used to design and document API's simultaneously, keeping the documentation in sync with the blueprint.

Swagger can be used to test APIs using Swagger Inspector. Swagger Inspector easily allows users to vaildate and test APIs with no limits on what is tested.

Swagger allows developers to generate client library code for different platforms enabling developers to build APIs at faster rate.

## How to setup Swagger in local for API Testing

Swagger provides a tool- Swagger Inspector for Testing APIs. The Swagger Inspector tool is available as an extension in chrome.

Go to the link below and add Swagger Inspector extension to chrome

https://chrome.google.com/webstore/detail/swagger-inspector-extensi/biemppheiopfggogojnfpkngdkchelik

Thats all that is needed to be able test APIs locally.

Note: Swagger UI is another tool provided by Swagger specifically to document APIs. Swagger UI can also be used to test APIs but if the purpose is to only test APIs then Swagger Inspector is the way to go.

## How to test APIs

You can test your API end points by pasting your end point at the top of the page.

You can make GET/POST/PUT/PATCH etc.. method calls to test your api accordingly.

Following is a sample POST request:

    1. Paste the end point in HTTP resource URL bar.
    2. Select the type of HTTP request, here it is POST.
    3. Provide the required parameters, Authentication & Headers (Basic Auth) and the body.
    4. Hit send and view the response below.
        i) A successful response should show a repsonse status 200.
       ii) If the reponse status is other than 200,then it indicates a failed scenario.

![](images/swagger-api-test-get-request.png)

Following is a sample GET request:

    1. Paste the end point in HTTP resource URL bar.
    2. Select the type of HTTP request, here it is GET.
    3. Provide the required parameters, Authentication & Headers (Basic Auth).
    4. Hit send and view the response below.
        i) A successful response should show a repsonse status 200.
       ii) If the reponse status is other than 200,then it indicates a failed scenario.

![](images/swagger-api-test-post-request.png)

All the test scenarios are saved in history tab. You can also create a collection by creating a collection and adding items to collection from history items.

## How to create documentation using Swagger

Swagger provides an easy way to document Rest API endpoints facilitating easy collaboration across teams and keeping the documentation in sync with codebase.

    1. First step is to add dependencies to your project

        i. Gradle users

            Add springfox swagger 2 dependency to build.gradle file:
                dependencies {
                    ...
                   implementation("io.springfox:springfox-swagger2:2.9.2")
                }
            In addition to provide a user interface for the docs, also add swagger UI dependency to build.gradle file:
                dependencies {
                    ...
	                implementation("io.springfox:springfox-swagger-ui:3.0.0")
                }
        
        ii. If you are a Maven user

            Add following dependencies to pom.xml file:
                <dependencies>
                    ...
                    <dependency>
                        <groupId>io.springfox</groupId>
                        <artifactId>springfox-swagger2</artifactId>
                        <version>2.9.2</version>
                    </dependency>
                    <dependency>
                            <groupId>io.springfox</groupId>
                            <artifactId>springfox-swagger-ui</artifactId>
                            <version>3.0.0</version>
                    </dependency>
                </dependencies>
    
    2. Provide a configuration class
        
        To integrate swagger 2 into the project, create a new configuration class which instructs springfox on how to use the project code.
        
        i.Create a class with name for example SwaggerConfiguration.
        ii. Add the following code into the class:
            
            @Configuration
            public class SpringFoxConfig {                                    
                @Bean
                public Docket api() {
                    return new Docket(DocumentationType.SWAGGER_2)  
                    .select()                                  
                    .apis(RequestHandlerSelectors.any())              
                    .paths(PathSelectors.any())                          
                    .build();                                           
                }
            }
    
        The docket object select method returns an instance of ApiSelectorBuilder providing a way to control API end points.
        More information on docket class can be found here: http://springfox.github.io/springfox/javadoc/2.7.0/springfox/documentation/spring/web/plugins/Docket.html
        
        Note: For plain Spring projects, the swagger 2 needs to be enabled explicitly. To do so, we need to provide @EnableSwagger2WebMvc on our configuration class:
                @Configuration
                @EnableSwagger2WebMvc
                public class SpringFoxConfig {                                    
                } 
    
    3. Once the above steps are successfully completed, to verify if the documentation is auto-generated, 
        i. run your application and visit http://localhost:8080/v2/api-docs (the address may vary depending on app configuration)
        ii. The result should be a json as below

![](images/swagger-doc-json.png)

    4. Since the json response is not very human readable, we use swagger-ui to view the same information but with a rich user interface. 
       i. We can test if swagger-ui is working by visiting http://localhost:8080/your-app-root/swagger-ui.html
       ii. Now, you can see a rich ui with your end points documented precisely.

![](images/swagger-ui-doc.png)

If you don't like the auto-generated descriptions in documentation, you can customize the same by using swagger annotations in your app controller.
More information to customize descriptions and advanced configurations can be found here:

https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api

https://springfox.github.io/springfox/javadoc/2.7.0/springfox/documentation/service/ApiInfo.html

## Additional information

More information on swagger can be found here:
https://swagger.io/docs/swagger-inspector/how-to-use-swagger-inspector/
