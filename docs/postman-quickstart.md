# Postman Quickstart Guide

Guide for API testing via Postman

## What is Postman?

Postman is an application that is used for API testing. It allows the user to submit HTTP requests, utilizing a graphic user interface, and get responses to validate APIs.

### Use Cases

Postman is a useful tool that can helpful in quickly designing and testing APIs.

As a tester, Postman can be used to try various requests allowing users to compare actual responses to expectations.

## How to setup locally

Download Postman via this link:
https://www.postman.com/downloads/

OR

Homebrew users run:
```bash
brew install postman
```

## How to test APIs
API's can be tested by selecting the Request Type and entering the endpoint to be tested (Note: Authorization may be required - 
See below for information on authorization).  
https://learning.postman.com/docs/sending-requests/requests/


Write test scripts for your APIs directly in Postman using JavaScript.


The following is an example of a test defined the Test Tab
```bash
pm.test("Status test", function () {
    pm.response.to.have.status(200);
});
```
https://learning.postman.com/docs/writing-scripts/intro-to-scripts/


Multiple tests can be placed in one folder, collection, or a single request.

Other code examples for tests and syntax:
https://learning.postman.com/docs/writing-scripts/script-references/test-examples/

## How to set up Auth

Postman allows various auth schemes:  
https://learning.postman.com/docs/sending-requests/authorization/

The following links describe how to use specific auth schemes.

### Digest Auth

https://learning.postman.com/docs/sending-requests/authorization/#digest-auth

### Basic Auth

https://learning.postman.com/docs/sending-requests/authorization/#basic-auth

### Bearer Token

https://learning.postman.com/docs/sending-requests/authorization/#bearer-token

## Links to documentation

Additional documentation can be found here:
https://learning.postman.com/docs/getting-started/introduction/
