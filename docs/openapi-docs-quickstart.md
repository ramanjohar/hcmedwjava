## OpenAPI (Swagger) Docs Plugin Quickstart Guide


### Configuring API Descriptor files:

The `apis` folder will have all the API descriptor files and `all-apis.yaml` file of type `Location`. `all-apis` defines relative locations of all the descriptor files under `spec.targets` property.

### What to include in API descriptor files?

Each API descriptor file should have the following properties defined : `apiVersion`, `kind`, `metadata`, `spec`. This section discusses the properties, along with examples of accepted values. 

- `apiVerion` - This is a required property and should be set to `backstage.io/v1alpha1`.


- `kind` - This is a required field and should be `API`.


- `metadata` - This includes the fields like name, description, tags, links, etc. `name` is a required field while including metadata and should follow these constraints:
    - Strings of length at least 1, and at most 63
    - Must consist of sequences of `[a-z0-9A-Z]` possibly separated by one of `[-_.]`.

      Ex: `test-java-service`, `CircleciBuildsDumpV2_avro_gcs`


- `spec` - Spec defines API properties like type, lifecycle, owner and definition. Following are the required fields while including spec.

    - `spec.type` - The type of the API definition as a string. Common set of known values for this are :
        - `openapi` - An API definition in YAML or JSON format based on the [OpenAPI](https://swagger.io/specification/) version 2 or version 3 spec.
        - `asyncapi` - An API definition based on the [AsyncAPI](https://www.asyncapi.com/docs/specifications/latest/) spec.
        - `graphql` - An API definition based on [GraphQL schemas](https://spec.graphql.org/) for consuming [GraphQL](https://graphql.org/) based APIs.
        - `grpc` - An API definition based on [Protocol Buffers](https://developers.google.com/protocol-buffers) to use with [gRPC](https://grpc.io/).

Note: This document discusses only **OpenAPI** configuration. For information on customizing other types see [here](https://github.com/backstage/backstage/tree/c2f3502ad79611ecbd9324790d43d0b0f9cc6ada/plugins/api-docs#customizations).


- `spec.lifecycle` - The lifecycle state of the API. Common values for this field are:
    - `experimental` - an experiment or early, non-production API, signaling that users may not prefer to consume it over other more established APIs, or that there are low or no reliability guarantees
    - `production` - an established, owned, maintained API
    - `deprecated` - an API that is at the end of its lifecycle, and may disappear at a later point in time


- `spec-owner` - An entity reference to the owner of the component.


- `spec-definition` - The definition of the API, based on the format defined by `spec.type`. It is possible to reference relative file paths and URLs. The descriptor format supports `$text`, `$json` and `$yaml`.

Sample `example-api.yaml` :

```
apiVersion: backstage.io/v1alpha1

kind: API

metadata:
  name: test-java
  description: This is a Java Spring Boot application that has been created using the DPS Starter Kit. It is intended to be used as a starting point for building Java APIs and should be customized to deliver whatever functionality is required. If no other changes have been made, this application will have these features included by default.
  tags:
    - OpenAPI
  links:
    - url: https://bitbucket.org/wwgrainger/zzz-testjava/src/master/
      title: Bitbucket Repo
      icon: bitbucket

spec:
  type: OpenAPI
  lifecycle: experimental
  owner: ProductCore
  definition: |
    openapi: 3.0.1
    info:
      title: OpenAPI definition
      version: v0
    servers:
      - url: https://zzz-test-java-v2-dev.nonprod.graingercloud.com
        description: Generated server url
    paths:
      "/todo":
        get:
          tags:
            - to-do-controller
          operationId: findAll
          responses:
            '200':
              description: OK
              content:
                "*/*":
                  schema:
                    type: object
        post:
          tags:
            - to-do-controller
          operationId: save
          requestBody:
            content:
              application/json:
                schema:
                  "$ref": "#/components/schemas/ToDoItem"
            required: true
          responses:
            '200':
              description: OK
              content:
                "*/*":
                  schema:
                    "$ref": "#/components/schemas/ToDoItem"
      "/":
        get:
          tags:
            - hello-world-controller
          operationId: hello
          responses:
            '200':
              description: OK
              content:
                "*/*":
                  schema:
                    type: string
        post:
          tags:
            - hello-world-controller
          operationId: hello1
          responses:
            '200':
              description: OK
              content:
                "*/*":
                  schema:
                    type: string
    components:
      schemas:
        ToDoItem:
          type: object
          properties:
            id:
              type: integer
              format: int64
            title:
              type: string
            done:
              type: boolean

```

### Referencing API in catalog-info 

Once the API descriptors are defined, they must be referenced in `catalog-info.yaml`. In `catalog-info.yaml`, add `providesApis`/`consumesApis` field under `spec`. The entries in `providesApis`/`consumesApis` are references. Accepted value is the `name` of an API (`metadata.name` in descriptor yaml files).

Ex :
```
spec:
  type: service
  system: grainger-backstage-dev
  owner: productcore
  lifecycle: production
  providesApis:
    - test-java-api
    - second-api
  consumesApis:
    - sample-api
```

### Accessing Swagger Docs On Backstage

1. Goto Backstage dev.
2. Click on **Register Existing API**.
3. Under _Select URL_ copy and paste the bitbucket url for the API's `config-info.yaml`.
4. Repeat steps 2 and 3 and register the `swagger-config.yaml` under `apis` folder.
5. Click on Analyze -> Refresh -> component:.
6. Goto API's tab. Here the API should be listed under **Provided APIs**.
7. Select the API and then click on **Definition** tab.
8. This tab should display _Swagger_ styled API Definition.
