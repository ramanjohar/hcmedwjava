package com.grainger.HCM.hcmedw.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;  

@RestController  
public class HelloWorldController   {
    @Value("${DEPLOYMENT_TYPE:}")
    private String deploymentType;

    @GetMapping("/")  
    public String hello()   {
        return (deploymentType != null && !deploymentType.equals("")) ? "Hello World " + deploymentType : "Hello World!";
    }  

    @PostMapping("/")  
    public String hello1()   {
        return "Hello World!";
    }
}
