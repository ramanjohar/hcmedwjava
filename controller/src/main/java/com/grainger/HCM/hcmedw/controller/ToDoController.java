package com.grainger.HCM.hcmedw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.grainger.HCM.hcmedw.service.provider.ToDoService;
import com.grainger.HCM.hcmedw.persistence.model.ToDoItem;


@RestController
@RequestMapping("/todo")
public class ToDoController {
    
    @Autowired
    private ToDoService toDoService;

    @GetMapping
    public Iterable<ToDoItem> findAll() {

        return toDoService.findAll();
    }

    @PostMapping
    public ToDoItem save(@RequestBody ToDoItem toDoItem) {

        return toDoService.save(toDoItem);
    }
}
