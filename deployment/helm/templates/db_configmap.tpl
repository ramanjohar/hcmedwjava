{{- if eq .Values.db.type "local" -}}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "productcore-java-starterkit-db.fullname" . }}
  labels:
    {{- include "productcore-java-starterkit.labels" . | nindent 4 }}
data:
  POSTGRES_DB: postgres
  POSTGRES_USER: postgresadmin
  POSTGRES_PASSWORD: admin123
{{- end }}
