{{- if eq .Values.db.type "local" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "productcore-java-starterkit-db.fullname" . }}
  labels:
    {{- include "productcore-java-starterkit-db.labels" . | nindent 4 }}
spec:
  replicas: 1
  selector:
    matchLabels:
      {{- include "productcore-java-starterkit-db.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "productcore-java-starterkit-db.labels" . | nindent 8 }}
    spec:
      containers:
        - name: postgres
          image: postgres:11.4
          imagePullPolicy: "IfNotPresent"
          ports:
            - containerPort: 5432
          envFrom:
            - configMapRef:
                name: {{ include "productcore-java-starterkit-db.fullname" . }}
          {{- if .Values.db.conf_override }}
          args:
            - -c
            - 'config_file=/etc/postgresql/postgresql.conf'
          volumeMounts:
            - mountPath: /etc/postgresql/postgresql.conf
              subPath: postgresql.conf
              name: postgresql-conf
          {{- end }}
      {{- if .Values.db.conf_override }}
      volumes:
        - name: postgresql-conf
          configMap:
            name: {{ include "productcore-java-starterkit-db.fullname" . }}-conf
      {{- end }}
{{- end }}
