{{ if and .Values.blue.enabled .Values.green.enabled }}
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: {{ include "productcore-java-starterkit.fullname" . }}
spec:
  host: {{ include "productcore-java-starterkit.fullname" . }}
  subsets:
    - name: prod
      labels:
        slot: {{ include "productcore-java-starterkit.productionSlot" . }}
    - name: test
      labels:
        slot: {{ include "productcore-java-starterkit.testSlot" . }}
{{ end }}
