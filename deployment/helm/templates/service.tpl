---
apiVersion: v1
kind: Service
metadata:
  name: {{ include "productcore-java-starterkit.fullname" . }}
  labels:
    {{- include "productcore-java-starterkit.labels" . | nindent 4 }}
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: http
      protocol: TCP
      name: http
  selector:
    {{- include "productcore-java-starterkit.selectorLabels" . | nindent 4 }}
