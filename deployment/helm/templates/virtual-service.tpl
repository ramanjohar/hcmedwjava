{{- if .Values.gateway.enabled }}
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: {{ include "productcore-java-starterkit.fullname" . }}
  labels:
    {{- include "productcore-java-starterkit.labels" . | nindent 4 }}
spec:
  hosts:
    - {{ .Values.fqdn }}
  gateways:
    - {{ include "productcore-java-starterkit.fullname" . }}
  http:
    {{ if and .Values.blue.enabled .Values.green.enabled }}
    - name: test
      match:
        - uri:
            prefix: "/test"
      rewrite:
        uri: "/"
      route:
        - destination:
            port:
              number: 80
            host: {{ include "productcore-java-starterkit.fullname" . }}
            subset: test
    {{ end }}
    - name: prod
      route:
        - destination:
            port:
              number: 80
            host: {{ include "productcore-java-starterkit.fullname" . }}
            {{ if and .Values.blue.enabled .Values.green.enabled }}
            subset: prod
            {{ end }}

{{- end }}