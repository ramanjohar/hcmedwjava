{{ if .Values.blue.enabled }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "productcore-java-starterkit.fullname" . }}-blue
  labels:
    {{- include "productcore-java-starterkit.labels" . | nindent 4 }}
    slot: blue
  {{- with .Values.deployment.blue.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  {{- if not .Values.deployment.blue.autoscaling.enabled }}
  replicas: {{ .Values.deployment.blue.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "productcore-java-starterkit.selectorLabels" . | nindent 6 }}
      slot: blue
  template:
    metadata:
      labels:
        {{- include "productcore-java-starterkit.labels" . | nindent 8 }}
        slot: blue
      annotations:
        {{- tpl ( .Values.deployment.blue.pod.annotations | toYaml ) . | nindent 8 }}
    spec:
      imagePullSecrets:
        {{- toYaml .Values.deployment.blue.imagePullSecret | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          {{- if not ( eq .Values.environment "local" ) }}
          command: [ "/bin/sh","-c" ]
          args: [ 'source /vault/secrets/db-creds && /usr/local/bin/entrypoint.sh' ]
          {{- end }}
          {{$data := dict "depObj" .Values.deployment.blue "Release" $.Release "Chart" $.Chart }}
          image: {{ include "productcore-java-starterkit.container" $data }}
          imagePullPolicy: {{ .Values.deployment.blue.image.pullPolicy }}
          ports:
            {{- toYaml .Values.deployment.blue.ports | nindent 12 }}
          {{- if hasKey .Values.deployment.blue "livenessProbe" }}
          livenessProbe:
            {{- toYaml .Values.deployment.blue.livenessProbe | nindent 12}}
          {{- end }}
          {{- if hasKey .Values.deployment.blue "readinessProbe" }}
          readinessProbe:
            {{- toYaml .Values.deployment.blue.readinessProbe | nindent 12}}
          {{- end }}
          env:
            - name: SKELETON_API_BASE_URL
              value: {{ .Values.scheme -}}
                ://
                {{- .Values.fqdn }}
            - name: SKELETON_API_DATABASE_HOST
              value: {{ include "productcore-java-starterkit-db.dbServiceHost" . }}
            - name: SKELETON_API_DATABASE_PORT
              value: {{ .Values.db.port | quote }}
            - name: SKELETON_API_DATABASE_ADAPTER
              value: postgres
              {{- if eq .Values.environment "local"  }}
            - name: SKELETON_API_DATABASE_USERNAME
              value: postgresadmin
            - name: SKELETON_API_DATABASE_PASSWORD
              value: admin123
              {{- end }}
          resources:
            {{- toYaml .Values.deployment.blue.resources | nindent 12 }}
{{ end }}
{{ if .Values.green.enabled }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "productcore-java-starterkit.fullname" . }}-green
  labels:
    {{- include "productcore-java-starterkit.labels" . | nindent 4 }}
    slot: green
  {{- with .Values.deployment.green.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  {{- if not .Values.deployment.green.autoscaling.enabled }}
  replicas: {{ .Values.deployment.green.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "productcore-java-starterkit.selectorLabels" . | nindent 6 }}
      slot: green
  template:
    metadata:
      labels:
        {{- include "productcore-java-starterkit.labels" . | nindent 8 }}
        slot: green
      annotations:
        {{- tpl ( .Values.deployment.green.pod.annotations | toYaml ) . | nindent 8 }}
    spec:
      imagePullSecrets:
        {{- toYaml .Values.deployment.green.imagePullSecret | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          {{- if not ( eq .Values.environment "local" ) }}
          command: [ "/bin/sh","-c" ]
          args: [ 'source /vault/secrets/db-creds && /usr/local/bin/entrypoint.sh' ]
          {{- end }}
          {{$data := dict "depObj" .Values.deployment.green "Release" $.Release "Chart" $.Chart }}
          image: {{ include "productcore-java-starterkit.container" $data }}
          imagePullPolicy: {{ .Values.deployment.green.image.pullPolicy }}
          ports:
            {{- toYaml .Values.deployment.green.ports | nindent 12 }}
          {{- if hasKey .Values.deployment.green "livenessProbe" }}
          livenessProbe:
            {{- toYaml .Values.deployment.green.livenessProbe | nindent 12}}
          {{- end }}
          {{- if hasKey .Values.deployment.green "readinessProbe" }}
          readinessProbe:
            {{- toYaml .Values.deployment.green.readinessProbe | nindent 12}}
          {{- end }}
          env:
            - name: DEPLOYMENT_TYPE
              value: green
            - name: SKELETON_API_BASE_URL
              value: {{ .Values.scheme -}}://{{- .Values.fqdn }}
            - name: SKELETON_API_DATABASE_HOST
              value: {{ include "productcore-java-starterkit-db.dbServiceHost" . }}
            - name: SKELETON_API_DATABASE_PORT
              value: {{ .Values.db.port | quote }}
            - name: SKELETON_API_DATABASE_ADAPTER
              value: postgres
              {{- if eq .Values.environment "local"  }}
            - name: SKELETON_API_DATABASE_USERNAME
              value: postgresadmin
            - name: SKELETON_API_DATABASE_PASSWORD
              value: admin123
              {{- end }}
          resources:
            {{- toYaml .Values.deployment.green.resources | nindent 12 }}
{{ end }}