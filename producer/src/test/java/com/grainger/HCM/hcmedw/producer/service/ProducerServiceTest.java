package com.grainger.HCM.hcmedw.producer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.apache.kafka.clients.producer.*;
import org.springframework.kafka.core.KafkaTemplate;
import com.grainger.HCM.hcmedw.data.producer.model.Message;

@SpringBootTest(classes = {ProducerService.class})
public class ProducerServiceTest {
    
    @Autowired
    private ProducerService producerService;

    @MockBean
    private KafkaTemplate<String, Message> producer;

    static String topic;

    static Message message;

    @BeforeAll
    public static void init() {
      topic = "mock.topic";
      message = new Message("ThisIsATest");
    }

    @BeforeEach
    void setUp() {
      ListenableFuture<SendResult<String, Message>> responseFuture = Mockito.mock(ListenableFuture.class);
      ProducerRecord<String, Message> record = new ProducerRecord<>(topic, "key", message);

      Mockito.when(producer.send(record)).thenReturn(responseFuture);
    }

    @Test
    public void sendMessage() {
      String success = "Published successfully!";
      String response = producerService.sendMessage("ThisIsATest");
      assertEquals(success, response);
    }
}
