package com.grainger.HCM.hcmedw.producer.config;

import org.apache.kafka.clients.producer.*;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;

import org.apache.kafka.clients.CommonClientConfigs;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.kafka.common.serialization.StringSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializer;

import com.grainger.HCM.hcmedw.data.account.model.AccountData;
import com.grainger.HCM.hcmedw.data.producer.model.Message;
import java.util.*;

@Configuration
public class KafkaProducerConfig {

    @Value("${spring.kafka.bootstrap.url}")
    private String brokerURL;

    @Value("${spring.kafka.schema.registry.url}")
    private String schemaURL;

    @Value("${spring.kafka.producer.security.protocol}")
    private String protocol;

    @Bean
    public ProducerFactory<String, Message> producerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokerURL);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        props.put("schema.registry.url", schemaURL);
        props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, protocol);

        return new DefaultKafkaProducerFactory<>(props);
    }

    @Bean
    public KafkaTemplate<String, Message> kafkaTemplate() {
        return new KafkaTemplate<String, Message>(producerFactory());
    }
}
