package com.grainger.HCM.hcmedw;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.ApplicationContext;

@Slf4j // lombok creates our logger as 'log' for us
@SpringBootApplication(scanBasePackages = {"com.grainger.HCM.hcmedw"})
public class ExampleApplication {

  /**
   * main function.
   *
   * @param args command line args
   */
  public static void main(String[] args) {
    new SpringApplication(ExampleApplication.class).run(args);
    log.info("\n\n\n\n\n---------------Example API Started.----------------\n\n\n\n\n");
  }

}
