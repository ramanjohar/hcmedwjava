package com.grainger.HCM.hcmedw.consumer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.apache.kafka.common.record.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.json.JSONObject;
import org.apache.kafka.clients.consumer.ConsumerRecord;

@SpringBootTest(classes = {ConsumerService.class})
public class ConsumerServiceTest {
    
    @Autowired
    private ConsumerService consumerService;

    private ConsumerRecord<String, Object> record;

    @BeforeEach
    void setUp() {
      JSONObject object = new JSONObject();
      object.put("message", "ThisIsATest");

      record = new ConsumerRecord<String, Object>("demo.mock.v1", 1, 0, 0L, TimestampType.CREATE_TIME, 0L, 0, 0, null, object);
    }

    @Test
    public void processMessage() throws Exception {
      consumerService.processMessage(record);
      assertEquals(consumerService.getLatch().getCount(), 99);
    }
}
