package com.grainger.HCM.hcmedw.data.subaccount.provider;

import com.grainger.starter.boot.test.data.provider.NamedDataFactory;
import com.grainger.starter.boot.test.data.spi.DataLoader;
import com.grainger.HCM.hcmedw.data.subaccount.model.SubAccountData;
import org.springframework.stereotype.Component;

@Component
public class SubAccountDataFactory extends NamedDataFactory<SubAccountData> {

  public SubAccountDataFactory(DataLoader<SubAccountData> loader) {
    super(loader);
  }
}
