package com.grainger.HCM.hcmedw.data.account.provider;

import com.grainger.starter.boot.test.data.provider.NamedDataFactory;
import com.grainger.starter.boot.test.data.spi.DataLoader;
import com.grainger.HCM.hcmedw.data.account.model.AccountData;
import org.springframework.stereotype.Component;

@Component
public class AccountDataFactory extends NamedDataFactory<AccountData> {

  public AccountDataFactory(DataLoader<AccountData> loader) {
    super(loader);
  }
}
