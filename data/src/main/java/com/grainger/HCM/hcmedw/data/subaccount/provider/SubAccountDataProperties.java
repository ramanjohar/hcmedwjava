package com.grainger.HCM.hcmedw.data.subaccount.provider;

import com.grainger.starter.boot.test.data.provider.GenericDataLoader;
import com.grainger.HCM.hcmedw.data.subaccount.model.SubAccountData;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("test-data.subaccount")
public class SubAccountDataProperties extends GenericDataLoader<SubAccountData> {}
