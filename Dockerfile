FROM adoptopenjdk/openjdk11:alpine-jre

ENV JAVA_ENABLE_DEBUG=${JAVA_ENABLE_DEBUG}
ENV DATADOG_CLIENT_VERSION=0.30.0
ENV JAR=app-0.1.0-SNAPSHOT.jar

RUN apk add --no-cache \
    curl~=7 \
    musl~=1 \
    libtasn1~=4 \
    libjpeg-turbo~=2

# Platform deployment
COPY /app/build/libs/${JAR} /usr/local/bin/
COPY /app/bin/entrypoint.sh /usr/local/bin/

RUN addgroup --system appuser -g 1001 && \
    adduser --system -g appuser -u 1001 appuser

RUN curl -L -o datadog-client.jar "https://search.maven.org/remotecontent?filepath=com/datadoghq/dd-java-agent/${DATADOG_CLIENT_VERSION}/dd-java-agent-${DATADOG_CLIENT_VERSION}.jar" && \
    chown appuser:appuser ./datadog-client.jar && \
    chmod 500 ./datadog-client.jar && \
    mv datadog-client.jar /usr/local/bin

RUN chown appuser:appuser /usr/local/bin/entrypoint.sh
RUN chmod 500 /usr/local/bin/entrypoint.sh

RUN chown appuser:appuser /usr/local/bin/${JAR}
RUN chmod 500 /usr/local/bin/${JAR}

EXPOSE 8080

USER 1001

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
